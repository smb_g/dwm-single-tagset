/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappx     = 4;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 1;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 0;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonclick		= 0;
static const char *fonts[]          = { "sans:size=9:style=Bold", "monospace:size=10", "FontAwesome:size=10", "Noto Color Emoji:size=10" };
static const char boldfont[]        = "monospace:size=10";
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#1F2430";
static const char col_gray2[]       = "#5C6773";
static const char col_white[]       = "#CBCCC6";
static const char col_green[]       = "#536743";
static const char col_cyan[]        = "#3e5670";
static const char col_red[]         = "#FF6666";
static const char *colors[][3]      = {
	/*                   fg         bg         border   */
	[SchemeNorm]  = { col_white, col_gray1,  col_gray1 },
	[SchemeCurr]   = { col_white, col_cyan,   col_cyan  },
	[SchemeSel]  = { col_white, col_green,  col_green },
	[SchemeFaded] = { col_gray2, col_gray1,  col_gray1 },
	[SchemeUrg]   = { col_gray1, col_red,    col_red   },
	[SchemeGreen] = { col_green, col_gray1,  col_gray1 },
	[SchemeBlue]  = { col_cyan,  col_gray1,  col_gray1 },
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                instance     title       tags mask     isfloating   monitor */
	{ "mpv",                  NULL,       NULL,       0,            1,           -1 },
	{ "feh",                  NULL,       NULL,       0,            1,           -1 },
	{ "Sxiv",                 NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",              NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Brave-browser",        NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Vivaldi-stable",       NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Opera",                NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Chromium",             NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Chromium-freeworld",   NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Sublime_text",         NULL,       NULL,       1 << 2,       0,           -1 },
	{ "Code - OSS",           NULL,       NULL,       1 << 2,       0,           -1 },
	{ "Deadbeef",             NULL,       NULL,       1 << 5,       0,           -1 },
	{ "Gimp",                 NULL,       NULL,       1 << 6,       0,           -1 },
	{ "discord",              NULL,       NULL,       1 << 7,       0,           -1 },
	{ "Thunderbird",          NULL,       NULL,       1 << 7,       0,           -1 },
	{ "Transmission-gtk",     NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const float mhfact    = 0.75; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ ""/*"[]="*/,      tile },    /* first entry is default */
	{ ""/*"[]="*/,      tiledM },    /* first entry is default */
	// { ""/*"[M]"*/,      centeredmaster },		/* three column layout */
	{ ""/*"[M]"*/,      monocle },
	// { ""/*"><>"*/,      NULL },    /* no layout function means floating behavior */
	{ NULL,				  NULL},
};


static const Tagrule tagrules[] = {
	/*tag   mfact   nmaster  layout -- 0 -> default*/
	{ 0,	0.65,	    0,   	0 },
	{ 1,	0,  	    0,   	2 },
	{ 2,	0.65,	    0,  	1 },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char scratchpadname[] = "spTerm";
static const char *scratchpadcmd[] = { "st", "-n", scratchpadname, "-g", "120x34", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_grave,  togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,           XK_Return, zoom,           {0} },
	{ MODKEY|ControlMask,           XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_q,      killallclients, {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_c,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_Tab,    cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Tab,    cyclelayout,    {.i = -1 } },
	{ MODKEY,                       XK_s,	   togglefloating, {0} },
	{ MODKEY,                       XK_f,      togglefullscreen, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY|ControlMask,           XK_k,	   focusmon,       {.i = -1 } },
	{ MODKEY|ControlMask,           XK_j, 	   focusmon,       {.i = +1 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ControlMask,           XK_q,      quit,           {0} },
	{ MODKEY|ControlMask,           XK_r,      quit,		   {1} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1 } },
	{ ClkLtSymbol,          0,              Button3,        cyclelayout,    {.i = -1 } },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
